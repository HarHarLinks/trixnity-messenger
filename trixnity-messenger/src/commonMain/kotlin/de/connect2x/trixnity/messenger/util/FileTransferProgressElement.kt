package de.connect2x.trixnity.messenger.util

data class FileTransferProgressElement(val percent: Float, val formattedProgress: String)